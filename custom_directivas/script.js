/*
Codigo facilito video 18
*/
angular.module('myApp', [])
.directive('backImg', function(){
    return function(scope, element, attrs){
        attrs.$observe('backImg', function(value){
            // value es la url
            element.css({
                "background": "url("+ value +")",
                "background-size": "cover",
                "background-position": "center"
            })
        })

    }
})
.controller('myController', function($scope, $http){
    $http.get('https://api.github.com/users/ahernandezfriz/repos')
    .success(function(data){
        $scope.repos = data;
    })
    .error(function(err){
        console.log(err);

    })
})
