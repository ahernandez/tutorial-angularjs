angular.module("FinalApp")
.controller("MainController", function($scope, $resource, PostResource){

    User = $resource("http://jsonplaceholder.typicode.com/users/:id", {id:
        "@id"});

    $scope.posts = PostResource.query()
    $scope.users = User.query()
    // query get post --> un arreglo de post


    // simular q se eliminan puesto q es una API de prueba
    $scope.removePost = function(post){
        PostResource.delete({id: post.id}, function(data){
            console.log(data)
        });

        $scope.posts = $scope.posts.filter(function(element){
            return element.id !== post.id;
        });
    }
 })
 .controller("PostController", function($scope, PostResource, $routeParams, $location){
     $scope.titulo = "Editar Post";
    $scope.post = PostResource.get({id: $routeParams.id});

    $scope.savePost = function(){
        PostResource.update({id: $scope.post.id},{data: $scope.post}, function(data){
            console.log(data);
            $location.path("/post/"+$scope.post.id);// redirecciona la post recien actualizado
        })
    }
 })
 .controller("NewPostController", function($scope, PostResource, $location){
    $scope.post = {};
    $scope.titulo = "Crear Post";

    // simula el ingreso, lo verificamos en log
    $scope.savePost = function(){
        PostResource.save({data: $scope.post}, function(data){
            console.log(data);
            $location.path("/");
        })
    }
 })
