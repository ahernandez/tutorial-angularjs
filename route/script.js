angular.module('myApp', ['ngRoute'])
.config(function($routeProvider){
    $routeProvider
        .when("/",{
            controller: "myController",
            templateUrl: "templates/home.html"
        })
        .when("/repo/:name", { //:name es una variable
            controller: "repoController", // nombre del controlador
            templateUrl: "templates/repo.html" // Ruta de la vista
        })
        .otherwise("/");
})
