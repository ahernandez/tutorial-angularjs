/*
 *    Ejemplo de To do List
 *    Aplicando Service

     Service se puede asemejar a un constructor en lenguajes de POO
==============================================================================*/
angular.module('toDoList', ['LocalStorageModule'])
    .service('toDoService', function(localStorageService){

        this.key = 'angular-todoList';

        if( localStorageService.get(this.key) ){
            this.activities = localStorageService.get(this.key);
        }else{
            this.activities = [];
        }

        this.add = function( newActividad ){
            this.activities.push( newActividad );
            this.uploadLocalStorage();
        }

        this.uploadLocalStorage = function(){
            localStorageService.set( this.key, this.activities )
        }

        this.clean = function(){
            this.activities = [];
            this.uploadLocalStorage();
            return this.getAll();
        }

        this.getAll = function(){
            return this.activities;
        }

        this.removeItem = function(item){
            this.activities = this.activities.filter(function(activity){
                return activity !== item
            });
            this.uploadLocalStorage();
            return this.getAll();
        }
    })

    .controller('controller1', function( $scope, toDoService ){

        $scope.todo = toDoService.getAll();
        $scope.newActividad = {};

        $scope.addActividad = function(){
            toDoService.add($scope.newActividad)
            $scope.newActividad = {};
        }

        $scope.removeActividad = function(item){
            $scope.todo = toDoService.removeItem(item);
        }

        $scope.clean = function(){
            $scope.todo = toDoService.clean();
        }
    })
