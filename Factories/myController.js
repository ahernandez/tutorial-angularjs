/*
 *    Ejemplo de To do List
 *    Aplicando Factory
==============================================================================*/
angular.module('toDoList', ['LocalStorageModule'])
    .factory('toDoService', function(localStorageService){

        var toDoService = {};
        toDoService.key = 'angular-todoList';

        if( localStorageService.get(toDoService.key) ){
            toDoService.activities = localStorageService.get(toDoService.key);
        }else{
            toDoService.activities = [];
        }

        toDoService.add = function( newActividad ){
            toDoService.activities.push( newActividad );
            toDoService.uploadLocalStorage();
        }

        toDoService.uploadLocalStorage = function(){
            localStorageService.set( toDoService.key, toDoService.activities )
        }

        toDoService.clean = function(){
            toDoService.activities = [];
            toDoService.uploadLocalStorage();
            return toDoService.getAll();
        }

        toDoService.getAll = function(){
            return toDoService.activities;
        }

        toDoService.removeItem = function(item){
            toDoService.activities = toDoService.activities.filter(function(activity){
                return activity !== item
            });
            toDoService.uploadLocalStorage();
            return toDoService.getAll();
        }

        return toDoService;
    })
    .controller('controller1', function( $scope, toDoService ){

        $scope.todo = toDoService.getAll();
        $scope.newActividad = {};

        $scope.addActividad = function(){
            toDoService.add($scope.newActividad)
            $scope.newActividad = {};
        }

        $scope.removeActividad = function(item){
            $scope.todo = toDoService.removeItem(item);
        }

        $scope.clean = function(){
            $scope.todo = toDoService.clean();
        }
    })
