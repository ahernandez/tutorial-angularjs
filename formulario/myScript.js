/*
Alumno controller
3 Alumnos por defecto


*/
angular.module('myApp',[]).controller('alumnosController', function($scope){
    $scope.alumnos = [
        {nombre:'ariel', telefono:'87878787', curso:'1 medio b'},
        {nombre:'ale', telefono:'8744444', curso:'1 medio a'},
        {nombre:'nicole', telefono:'5444780', curso:'2 medio a'},
    ]

    /*
    Añade un nuevo Alumno a la lista
    */
    $scope.save = function(){

        $scope.alumnos.push({
            nombre:   $scope.nuevoAlumno.nombre,
            telefono: $scope.nuevoAlumno.telefono,
            curso:    $scope.nuevoAlumno.curso,
        })
        $scope.hideForm = false;
    }

    $scope.hideForm = false;
    $scope.showForm = function(){
        $scope.hideForm = true;
        console.log($scope.hideForm);
    }


})
