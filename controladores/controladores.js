angular.module('myApp', [])
.run(function($rootScope){
    // Su utilidad es como variable global ya que nos permite añadir propiedades
    // a él y que el resto de controladores tengan acceso.
    $rootScope.nombre ="Nicole";
})
.controller('miControlador', function($scope){

    // variables
    $scope.nombre = "Ariel";
    $scope.aPaterno="Hernández";
    $scope.aMaterno="Friz"
})
.controller('SegundoController', function($scope){
    $scope.nombre = "Alejandro";
})
