angular.module('myApp', ['LocalStorageModule'])
.controller('myController', function($scope, localStorageService){

    /*
    descripcion: 'Aprendiendo AngularJS'
    Fecha: 21-10-2015 10:28pm
    */
    if( localStorageService.get("angular-todoList") ){
        $scope.todo = localStorageService.get("angular-todoList");
    }else{
        $scope.todo = [];
    }

    $scope.$watchCollection('todo', function(nuevo, viejo){
        localStorageService.set("angular-todoList", $scope.todo);
    })


    $scope.addActividad = function(){
        $scope.todo.push( $scope.newActividad);
        $scope.newActividad ={};
    };

    /*
    funciona junto a ng-click="limpiar()"
    $scope.limpiar = function(){
        $scope.todo = [];
    }
    */

});
