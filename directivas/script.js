angular.module('myApp', [])
.controller('myController', function($scope, $http){
    $http.get('https://api.github.com/users/ahernandezfriz/repos')
    .success(function(data){
        $scope.repos = data;
    })
    .error(function(err){
        console.log(err);

    })
})
