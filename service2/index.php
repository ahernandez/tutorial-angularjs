<!DOCTYPE html>
<html ng-app="toDoList" lang="en">
<head>
    <meta charset="UTF-8">
    <title>ADD local-storage</title>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0-beta.0/angular.min.js"></script>
    <script src="../modulos/angular-local-storage.min.js"></script>
    <script src="script.js"></script>
</head>
<body ng-controller="controller1">
    <ul>
        <li ng-repeat="actividad in todo">
            {{actividad.descripcion}} - <small>{{actividad.fecha | date: 'short'}}</small> ---
            <a href="" ng-click="removeActividad(actividad)"><b>Eliminar</b></a>
        </li>
    </ul>
    <br>
    <form ng-submit="addActividad()">
        <input type="text" ng-model="newActividad.descripcion">
        <input type="datetime-local" ng-model="newActividad.fecha">
        <input type="submit" value="Agregar">
        <!-- <button ng-click="limpiar()">Limpiar/Borrar</button> otra alternativa -->
        <button ng-click="clean()">Limpiar/Borrar</button>
    </form>

</body>
</html>
