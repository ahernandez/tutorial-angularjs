/*
 Otra manera de Inicializar el controlador
 ========================================================
var app = angular.module('invoice1', []);
app.controller('InvoiceController', function(){
    aqui code
});
*/

angular.module('invoice1', []).controller('InvoiceController', function() {
    this.qty = 0;
    this.cost = 0;
    this.valor = 2;
    this.inCurr = 'EUR';
    this.currencies = ['USD', 'EUR', 'CNY'];
    this.usdToForeignRates = {
        USD: 680,
        EUR: 720,
        CNY: 2000
    };

    this.total = function total(outCurr) {
        return this.convertCurrency(this.qty * this.cost, this.inCurr, outCurr);
    };

    this.convertCurrency = function convertCurrency(amount, inCurr, outCurr) {
        return amount * this.usdToForeignRates[outCurr] / this.usdToForeignRates[inCurr];
    };

    this.pay = function pay() {
        window.alert("Thanks!");
    };
});
